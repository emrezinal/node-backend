0)install env-cmd
1)create test.env file inside config folder alongside dev.env (use seperate db to test)
2)update test command: "test": "env-cmd ./config/test.env jest --watch"
3)add jest config to package.json
"jest": {
    "testEnvironment": "node"
  },
4-pre)run ./node_modules/.bin/env-cmd -f ./server/config/dev.env node index.js
4-pre2)move routes to routes folder
4)add app.js near index.js and remove app.listen from app.js: when we use supertest we will require the one without app.listen (app.js)
app.js:
    const express = require('express')
    require('./db/mongoose')
    const userRouter = require('./routers/user')
    const taskRouter = require('./routers/task')

    const app = express()

    app.use(express.json())
    app.use(userRouter)
    app.use(taskRouter)

    module.exports = app  
index.js:    
    const app = require('./app')
    const port = process.env.PORT

    app.listen(port, () => {
        console.log('Server is up on port ' + port)
    })
5)More test ideas
//
// User Test Ideas
//
// Should not signup user with invalid name/email/password
// Should not update user if unauthenticated
// Should not update user with invalid name/email/password
// Should not delete user if unauthenticated

//
// Task Test Ideas
//
// Should not create task with invalid description/completed
// Should not update task with invalid description/completed
// Should delete user task
// Should not delete task if unauthenticated
// Should not update other users task
// Should fetch user task by id
// Should not fetch user task by id if unauthenticated
// Should not fetch other users task by id
// Should fetch only completed tasks
// Should fetch only incomplete tasks
// Should sort tasks by description/completed/createdAt/updatedAt
// Should fetch page of tasks