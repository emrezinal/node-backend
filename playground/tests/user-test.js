const request = require('supertest');

const { User } = require('../src/models/user');
const app = require('../app'); //load in app.js (the one without app.listen)
const { userOne, userOneId, setupDatabase } = require('./fixtures/db');

//recreate db everytime tests runs
beforeEach(setupDatabase)

// afterEach(() => {
//     console.log('after each');

// })

test('should signup a new user', async() => {
    const response = await request(app).post('/users').send({
        name: "emre",
        email: "emre@gmail.com",
        password: 'mypass7812390'
    }).expect(201);

    //assert that db changed correctly
    const user = await User.findById(response.body.user._id);
    expect(user).not.toBeNull()

    //assertions about the response
    // expect(response.body.user.name).toBe('emre')
    expect(response.body).toMatchObject({
        user: {
            name: 'emre',
            email: 'emre@gmail.com'
        },
        tokens: user.tokens[0].token
    })
    expect(user.password).not.toBe('mypass7812390')
});

test('should login existing user', async() => {
    const response = await request(app).post('/users/login').send({
        email: userOne.email,
        password: userOne.password
    }).expect(200);

    //fetch user from db
    const user = await User.findById(response.body.user._id);
    expect(user).not.toBeNull()

    expect(response.body.token).toBe(user.tokens[1].token);
});

test('should not login nonexistent user', async() => {
    await request(app).post('/users/login').send({
        email: userOne.email,
        password: 'mypass7812390'
    }).expect(400)
})

//Uses authorization
test('should get profile for user', async() => {
    await request(app)
        .get('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200);
})

test('Should not get profile for unauthenticated user', async() => {
    await request(app)
        .get('/users/me')
        .send()
        .expect(401);
})

test('Should delete account for authenticated user', async() => {
    await request(app)
        .delete('/user/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send(userOneId)
        .expect(200);
    const user = await User.findById(userOneId);
    expect(user).toBeNull();
})

test('Should not delete account for authenticated user', async() => {
    await request(app)
        .delete('/users/me')
        .send(userOneId)
        .expect(401);
})

//Test file upload
test('should upload avatar image', async() => {
    await request(app)
        .post('/users/me/avatar')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .attach('avatar', 'tests/fixtures/profile-pic.jpg')
        .expect(200);

    const user = await User.findById(userOneId);
    //we cant compare two objects using toBe,so use toEqual
    expect(user.avatar).toEqual(expect.any(Buffer));
})

test('should update valid user fields', async() => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            name: 'Jess'
        })
        .expect(200);
    const user = await User.findById(userOneId);
    expect(user.name).toEqual('Jess');
})

test('should not update invalid user fields', async() => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            location: "Philly" //there is no field in schema
        })
        .expect(400);
})