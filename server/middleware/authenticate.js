const { User } = require('../user/user.model');

const authenticate = (req, res, next) => {
  const token = req.header('x-auth');

  User.findByToken(token).then((user) => {
    if (!user) {
      return Promise.reject();
    }
    req.user = user;
    req.token = token;
    next();
  }).catch((e) => {
    res.status(401).json({
      err: 'You are not authorized to make this request!',
    }); // 401 : unauthorized
  });
};

module.exports = {
  authenticate,
};
