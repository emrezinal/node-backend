const express = require('express');
const bodyParser = require('body-parser');
require('./db/mongoose');

const userRouter = require('./routers/user');
const categoryRouter = require('./routers/category');
const questionRouter = require('./routers/question');
const surveyRouter = require('./routers/survey');

const app = express();

app.use(bodyParser.json());
app.use(surveyRouter);
app.use(userRouter);
app.use(categoryRouter);
app.use(questionRouter);

// app.listen(port, () => {
//     console.log(`Listening on port ${port}`);
// }); 

module.exports = app;