const { Survey } = require('./survey.model');
const { Question } = require('./../question/question.model');
const { ObjectID } = require('mongodb');
const _ = require('lodash');

module.exports.add = function(req, res, next) {
    var survey = new Survey(req.body);
    survey.save().then((doc) => {
        return res.status(200).json({
            message: "New Survey has been added successfully.",
            data: doc
        });
    }, (e) => {
        console.log(e);
        e.status = '400';
        return next(e);
    });
};

module.exports.show = function(req, res, next) {
    let id = req.params.id;
    if (!ObjectID.isValid(id)) {
        console.log('Id not valid');
        return res.status(400).json({
            err: "Given survey id is not a valid id!"
        });
    }
    Survey.findById(id).then((survey) => {
        if (!survey) {
            return res.status(404).json({
                err: "No such survey with the given id!"
            });
        }
        res.send({ survey });
    }).catch(
        (e) => { res.status(400).send(); }
    );
};

module.exports.edit = function(req, res, next) {
    let id = req.params.id;
    if (!ObjectID.isValid(id)) {
        console.log('Id not valid');
        return res.status(400).json({
            err: "Given survey id is not a valid id!"
        });
    }
    Survey.findByIdAndUpdate(id, { $set: req.body }, { new: true }).then((survey) => {
        if (!survey)
            return res.status(404).json({
                err: "There is no such survey with the given id!"
            });
        return res.status(200).json({
            message: "Survey updated",
            data: survey
        });
    }).catch((e) => {
        console.log(e);
        e.status = '400';
        return next(e);
    });
};

module.exports.insertQuestion = function(req, res, next) {
    let sid = req.params.sid; //survey id
    let qid = req.params.qid; //question id
    if (!ObjectID.isValid(sid)) {
        console.log('Survey Id not valid');
        return res.status(400).json({
            err: "Given survey id is not a valid id!"
        });
    }
    if (!ObjectID.isValid(qid)) {
        console.log('Question Id not valid');
        return res.status(400).json({
            err: "Given question id is not a valid id!"
        });
    }

    //verify there is question with given id
    Question.findById(qid).then((question) => {
        if (!question) {
            return res.status(404).json({
                err: "There is no such question with the given id!"
            });
        }
        let newQuestionId = _.pick(question, ['_id'])._id;
        console.log('Got question:', newQuestionId);
        Survey.findOneAndUpdate({ _id: sid }, { $push: { questions: newQuestionId } }, { new: true }).then((survey) => {
            if (!survey)
                return res.status(404).json({
                    err: "There is no such survey with the given id!"
                });
            return res.status(200).json({
                message: "Inserted new question to survey",
                qid: newQuestionId,
                survey: survey
            });
        }).catch((e) => {
            console.log(e);
            e.status = '400';
            return next(e);
        });
    }).catch(
        (e) => { res.status(400).send(); }
    );
};

module.exports.delete = function(req, res, next) {
    let id = req.params.id;
    if (!ObjectID.isValid(id)) {
        console.log('Id not valid');
        return res.status(400).json({
            err: "Given survey id is not a valid id!"
        });
    }
    Survey.findByIdAndRemove(id).then((doc) => {
        if (!doc) {
            return res.status(404).json({
                err: "There is no survey with the given id!"
            });
        }
        res.status(200).send(doc);
    }).catch((e) => {
        return res.status(400).send();
    });
};