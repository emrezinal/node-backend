const express = require('express');

const SurveyController = require('./../survey/survey.controller');

const router = new express.Router();

// Survey API
router.post('/survey/:sid/:qid', SurveyController.insertQuestion); // add question (qid) to survey (sid) //TODO:ADMIN PRIVILEGES REQUIRED
router.post('/survey', SurveyController.add); // create a new test //TODO:ADMIN PRIVILEGES REQUIRED
router.get('/survey/:id', SurveyController.show); // get a test by id
router.delete('/survey/:id', SurveyController.delete); // delete a test by id //TODO:ADMIN PRIVILEGES REQUIRED
router.patch('/survey/:id', SurveyController.edit); // update a test  //TODO:ADMIN PRIVILEGES REQUIRED

module.exports = router;
