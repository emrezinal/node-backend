const express = require('express');

const QuestionController = require('./../question/question.controller');

const router = new express.Router();

// Question API
router.get('/question/:id', QuestionController.getById); // get individual question by id
router.get('/question/showAll', QuestionController.showAll); // show all questions //TODO:ADMIN PRIVILEGES REQUIRED
router.post('/question', QuestionController.add); // add a question //TODO:ADMIN PRIVILEGES REQUIRED
router.patch('/question/:id', QuestionController.edit); // edit a question //TODO:ADMIN PRIVILEGES REQUIRED
router.delete('/question/:id', QuestionController.delete); // delete a question //TODO:ADMIN PRIVILEGES REQUIRED

module.exports = router;
