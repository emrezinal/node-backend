const express = require('express');

const CategoryController = require('./../category/category.controller');
const { authenticate } = require('./../middleware/authenticate');

const router = new express.Router();

// Categories API
router.post('/category/:cid/:sid', CategoryController.insertSurvey); // add survey (sid) to category (cid) //TODO:ADMIN PRIVILEGES REQUIRED
router.post('/category', CategoryController.add); // add a new category //TODO:ADMIN PRIVILEGES REQUIRED
router.get('/category/showAll', authenticate, CategoryController.showAll); // get all categories
router.get('/category/:id', authenticate, CategoryController.get); // get surveys in a single category
router.patch('/category/:id', CategoryController.edit); // update existing category //TODO:ADMIN PRIVILEGES REQUIRED
router.delete('/category/:id', CategoryController.delete); // delete existing category //TODO:ADMIN PRIVILEGES REQUIRED

module.exports = router;
