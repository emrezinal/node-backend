const request = require('supertest');
const app = require('../app');
const { Question } = require('./../question/question.model');
const {
  questionOne,
  questionOneId,
  questionTwo,
  questionTwoId,
  setupDatabase,
} = require('./fixtures/db');

// save questionOne and questionTwo beforehand
beforeEach(setupDatabase);

// Should get question by id
test('Should get individual question by id', async () => {
  await request(app)
    .get(`/question/${questionOneId}`)
    .send()
    .expect(200);
});

test('Should add a new question', async () => {
  await request(app)
    .post('/question')
    .send({
      questionText: 'choose one..',
      options: ['a', 'b', 'c'],
      answer: 2,
    })
    .expect(200);
});

test('Should update a question', async () => {
  await request(app)
    .patch(`/question/${questionOneId}`)
    .send({
      questionText: 'choose one..',
    })
    .expect(200);
});

test('Should delete a question', async () => {
  await request(app)
    .delete(`/question/${questionOneId}`)
    .send()
    .expect(200);
  const question = await Question.findById(questionOneId);
  expect(question).toBeNull();
});
