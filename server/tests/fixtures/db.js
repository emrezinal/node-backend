const ObjectId = require('mongodb').ObjectID;
const jwt = require('jsonwebtoken');
const { User } = require('../../user/user.model');
const { Category } = require('../../category/category.model');
const { Question } = require('../../question/question.model');
const { Survey } = require('../../survey/survey.model');

const userOneId = new ObjectId();
const userOne = {
  _id: userOneId,
  name: 'emre',
  surname: 'zinal',
  gender: 'male',
  email: 'emre@gmail.com',
  password: 'mypass7812390',
  tokens: [{
    access: 'auth',
    token: jwt.sign({ _id: userOneId, access: 'auth' }, process.env.JWT_SECRET).toString(),
  }],
};

const userTwoId = new ObjectId();
const userTwo = {
  _id: userTwoId,
  name: 'mehmet',
  surname: 'aslamaci',
  gender: 'male',
  email: 'memet@gmail.com',
  password: 'asd390',
  tokens: [{
    access: 'auth',
    token: jwt.sign({ _id: userTwoId, access: 'auth' }, process.env.JWT_SECRET).toString()
  }],
};

const questionOneId = new ObjectId();
const questionOne = {
  _id: questionOneId,
  questionText: 'question one text',
  options: ['a', 'b', 'c'],
  answer: 2,
};
const questionTwoId = new ObjectId();
const questionTwo = {
  _id: questionTwoId,
  questionText: 'question two text',
  options: ['a', 'b', 'd'],
  answer: 2,
};

const categoryOneId = new ObjectId();
const categoryOne = {
  _id: categoryOneId,
  categoryName: 'cat1',
};

const categoryTwoId = new ObjectId();
const categoryTwo = {
  _id: categoryTwoId,
  categoryName: 'cat2',
};

const surveyOneId = new ObjectId();
const surveyOne = {
  _id: surveyOneId,
  surveyName: 'survey1',
};

const surveyTwoId = new ObjectId();
const surveyTwo = {
  _id: surveyTwoId,
  surveyName: 'survey2',
};

const setupDatabase = async () => {
  await User.deleteMany();
  await Question.deleteMany();
  await Category.deleteMany();
  await Survey.deleteMany();
  await new User(userOne).save();
  await new User(userTwo).save();
  await new Question(questionOne).save();
  await new Question(questionTwo).save();
  await new Category(categoryOne).save();
  await new Category(categoryTwo).save();
  await new Survey(surveyOne).save();
  await new Survey(surveyTwo).save();
};

module.exports = {
  userOne,
  userOneId,
  userTwo,
  userTwoId,
  questionOne,
  questionOneId,
  questionTwo,
  questionTwoId,
  categoryOneId,
  categoryOne,
  categoryTwoId,
  categoryTwo,
  surveyOne,
  surveyOneId,
  surveyTwo,
  surveyTwoId,
  setupDatabase,
};
