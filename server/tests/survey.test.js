const request = require('supertest');
const app = require('../app');
const { Survey } = require('./../survey/survey.model');
const {
  questionOneId,
  surveyOne,
  surveyOneId,
  surveyTwo,
  surveyTwoId,
  setupDatabase,
} = require('./fixtures/db');

// save questionOne and questionTwo beforehand
beforeEach(setupDatabase);

test('Should add a new survey', async () => {
  await request(app)
    .post('/survey')
    .send({
      surveyName: 'a new survey',
    })
    .expect(200);
});

test('Should get individual survey by id', async () => {
  await request(app)
    .get(`/survey/${surveyOneId}`)
    .send()
    .expect(200);
});

test('Should edit a given survey', async () => {
  await request(app)
    .patch(`/survey/${surveyOneId}`)
    .send({
      surveyName: 'new name',
    })
    .expect(200);
});

test('Should delete a survey', async () => {
  await request(app)
    .delete(`/survey/${surveyOneId}`)
    .send()
    .expect(200);
});

test('Should add question to the survey', async () => {
  await request(app)
    .post(`/survey/${surveyTwoId}/${questionOneId}`)
    .send()
    .expect(200);  
});
