const request = require('supertest');
const app = require('../app');
const { User } = require('../user/user.model');
const {
  userOne,
  userTwoId,
  userTwo,
  setupDatabase,
} = require('./fixtures/db');

beforeEach(setupDatabase);

// Should signup a new user
test('Should signup a new user', async() => {
  const response = await request(app)
    .post('/user')
    .send({
      name: 'Andrew',
      surname: 'jess',
      gender: 'male',
      email: 'andrew@example.com',
      password: 'MyPass777!',
    })
    .expect(200);

  // Assert that the database was changed correctly
  const user = await User.findById(response.body._id);
  expect(user).not.toBeNull();

  // Assertions about the responseasync
  // expect(response.body).toMatchObject({
  //     user: {
  //         name: 'Andrew',
  //         email: 'andrew@example.com'
  //     },
  //     token: user.tokens[0].toasync
  // });
  // expect password to be hashed async
  expect(user.password).not.toBe('MyPass777!');
});

// Should login existing user
// test('Should login existing user', async() => {
//     const response = await request(app).post('/user/login').send({
//         email: userOne.email,
//         password: userOne.password
//     }).expect(200)
//     const user = await User.findById(userOneId);

//     // expect(response.body.token).toBe(user.tokens[1].token);
// })

// Should not login nonexistent user
test('Should not login user with invalid credentials', async() => {
  await request(app)
    .post('/user/login')
    .send({
      email: userOne.email,
      password: 'thisisnotmypass',
    })
    .expect(400);
});
// Should get profile for user
test('Should get profile for authenticated user', async() => {
  await request(app)
    .get('/user/me')
    .set('x-auth', userOne.tokens[0].token)
    .send()
    .expect(200);
});
// Should not get profile for unauthenticated user
test('Should not get profile for unauthenticated user', async () => {
  await request(app)
    .get('/user/me')
    .send()
    .expect(401);
});
// Should delete account for user
test('Should delete account for authenticated user', async() => {
  await request(app)
    .delete(`/user/${userTwoId}`)
    .set('x-auth', userTwo.tokens[0].token)
    .send()
    .expect(200);
  const user = await User.findById(userTwoId);
  expect(user).toBeNull();
});
// Should not delete account for unauthenticated user
test('Should not delete account for unauthenticate user', async() => {
  await request(app)
    .delete(`/user/${userTwoId}`)
    .send()
    .expect(401);
});
// Should update valid user fields
test('Should update valid user fields', async () => {
  await request(app)
    .patch(`/user/${userTwoId}`)
    .set('x-auth', userTwo.tokens[0].token)
    .send({
      name: 'Jess',
    })
    .expect(200);
  const user = await User.findById(userTwoId);
  expect(user.name).toEqual('Jess');
});

// Should not update invalid user fields
test('Should not update invalid user fields', async() => {
  await request(app)
    .patch(`/user/${userTwoId}`)
    .set('x-auth', userTwo.tokens[0].token)
    .send({
      location: 'Philadelphia',
    })
    .expect(400);
});
// User shouldn't be able to login after they delete their accounts
test("Users shouldn't be able to login after they delete their accounts", async () => {
  await request(app)
    .delete(`/user/${userTwoId}`)
    .set('x-auth', userTwo.tokens[0].token)
    .send()
    .expect(200);
  await request(app)
    .post('/user/login')
    .send({
      email: userTwo.email,
      password: userTwo.password,
    })
    .expect(400);
  const user = await User.findById(userTwoId);
  expect(user).toBeNull();
});