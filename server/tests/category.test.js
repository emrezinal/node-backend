const request = require('supertest');
const app = require('../app');
const { Category } = require('./../category/category.model');
const {
  userOne,
  surveyTwoId,
  categoryOneId,
  categoryOne,
  categoryTwoId,
  categoryTwo,
  setupDatabase,
} = require('./fixtures/db');

beforeEach(setupDatabase);

test('Should add a new category', async () => {
  await request(app)
    .post('/category')
    .send({
      categoryName: 'a new category',
    })
    .expect(200);
});

test('Should get individual category by id', async () => {
  await request(app)
    .get(`/category/${categoryOneId}`)
    .set('x-auth', userOne.tokens[0].token)
    .send()
    .expect(200);
});

test('Should get all categories', async () => {
  await request(app)
    .get('/category/showAll')
    .set('x-auth', userOne.tokens[0].token)
    .send()
    .expect(200);
});

test('Should edit a category given id', async () => {
  await request(app)
    .patch(`/category/${categoryOneId}`)
    .send({
      categoryName: 'dog1',
    })
    .expect(200);
});

test('Should delete a category given id', async () => {
  await request(app)
    .delete(`/category/${categoryOneId}`)
    .send()
    .expect(200);
});

test('Should add survey to the category', async () => {
  await request(app)
    .post(`/category/${categoryTwoId}/${surveyTwoId}`)
    .send()
    .expect(200);
});
