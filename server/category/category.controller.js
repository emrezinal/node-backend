const { Category } = require('./category.model');
const { Survey } = require('./../survey/survey.model');
const { ObjectID } = require('mongodb');
const _ = require('lodash');

module.exports.add = function(req, res, next) {
  var category = new Category(req.body);
  category.save().then((doc) => {
    return res.status(200).json({
      message: "New Category has been added successfully.",
      data: doc
    });
  }, (e) => {
    console.log(e);
    e.status = '400';
    return next(e);
  });
};

module.exports.get = function(req, res, next) {
  let id = req.params.id;
  Category.findById(id).then((category) => {
    if (!category)
      return console.log('Category id not found');
    return res.status(200).json({
      message: "Category found",
      data: category
    });
  }).catch((e) => console.log(e));
};

module.exports.showAll = function(req, res, next) {
  Category.find().then((categories) => {
    if (categories === undefined || categories.length == 0) {
      return res.status(404).json({
        err: "No categories yet"
      });
    }
    return res.status(200).json({
      message: "Categories found",
      data: categories
    });
  }, (e) => {
    console.log(e);
    e.status = '400';
    return next(e);
  });
};

module.exports.edit = function(req, res, next) {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    console.log('Id not valid');
    return res.status(400).json({
      err: "Given category id is not a valid id!"
    });
  }
  Category.findByIdAndUpdate(id, { $set: req.body }, { new: true }).then((category) => {
    if (!category)
      return res.status(404).json({
        err: "There is no category with the given id!"
      });
    return res.status(200).json({
      message: "Category updated",
      data: category
    });
  }).catch((e) => {
    console.log(e);
    e.status = '400';
    return next(e);
  });
};

module.exports.insertSurvey = function(req, res, next) {
  let cid = req.params.cid;
  let sid = req.params.sid;
  if (!ObjectID.isValid(cid)) {
    console.log('Category Id not valid');
    return res.status(400).json({
      err: "Given category id is not a valid id!"
    });
  }
  if (!ObjectID.isValid(sid)) {
    console.log('Survey id not valid');
    return res.status(400).json({
      err: "Given survey id is not a valid id!"
    });
  }

  //verify there is survey with given id
  Survey.findById(sid).then((survey) => {
    if (!survey)
      return res.status(404).json({
        err: "There is no such survey with the given id!"
      });
    let newSurveyId = _.pick(survey, ['_id'])._id;
    Category.findByIdAndUpdate(cid, { $push: { surveys: newSurveyId } }, { new: true }).then((category) => {
      if (!category) {
        return res.status(400).json({
          err: "There is no such category with the given id!"
        });
      }
      return res.status(200).json({
        message: "Inserted new survey to category",
        sid: newSurveyId,
        data: category
      });
    }).catch((e) => {
      console.log(e);
      e.status = '400';
      return next(e);
    });
  }).catch(
    (e) => { res.status(400).send(); }
  );
};

module.exports.delete = function(req, res, next) {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    console.log('Id not valid');
    return res.status(400).json({
      err: "Given category id is not a valid id!"
    });
  }
  Category.findByIdAndRemove(id).then((doc) => {
    if (!doc) {
      return res.status(404).json({
        err: "There is no such category by the given id to remove!"
      });
    }
    res.status(200).send(doc);
  }).catch((e) => {
    return res.status(400).send();
  });
};
